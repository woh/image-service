require('dotenv').config({ debug: process.env.DEBUG });

const express = require('express');
const mongodb = require('mongodb');
const axios = require('axios');
const sharp = require('sharp');

const { MongoClient } = mongodb;
const app = express();
const mongo = new MongoClient(`mongodb://${process.env.MONGODB_HOST}:27017`, { useNewUrlParser: true, useUnifiedTopology: true });

mongo.connect((err) => {
  if (err) {
    throw err;
  }

  const mongodbDb = mongo.db(process.env.MONGODB_DB);
  const gridfsBucket = new mongodb.GridFSBucket(mongodbDb);

  app.get(/.*/, async (req, res) => {
    let url = req.originalUrl;
    if (url.startsWith('/')) {
      url = url.substring(1);
    }

    let imgStream;
    try {
      imgStream = await axios({ method: 'get', url, responseType: 'stream' });
    } catch (e) {
      res.send(e);

      throw e;
    }

    const rejpeg = sharp().jpeg();
    const gridfsStream = gridfsBucket.openUploadStream(url, { contentType: 'image/jpeg' });

    imgStream.data.pipe(rejpeg).pipe(gridfsStream);

    res.send(gridfsStream.id);
  });

  app.listen(process.env.PORT, () => {
    console.log('HTTP Server ready');
  });
});
